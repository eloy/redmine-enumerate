#!/usr/bin/env python3
import requests
from bs4 import BeautifulSoup

url = "https://osmocom.org/users/"

#for i in range(0,100000):
for i in range(0,100000):
    current_url = url + str(i)
    page = requests.get(current_url)
    if "The page you were trying to access doesn" in page.text:
        continue
    soup = BeautifulSoup(page.content, "html.parser")
    results = soup.find(id="content")
    job_elements = results.find_all("li", class_="list_cf cf_9") 
    for job_element in job_elements:
        print(job_element, end="\n"*2)
