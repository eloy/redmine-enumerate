# Enumerate Redmine user licenses
Only redmine wiki administrators can see the list of users, so I quickly wrote this to give an overview of licenses used by Redmine users. Redmine user IDs are rather short, but the information is seemed public. It's kind of an IDOR, but not a particular interesting one :)

Redmine user IDs seems to be int4 aka 32 bit, but they are surprisingly short most of the time. 

https://www.redmine.org/projects/redmine/wiki/DatabaseModel
